<header class="">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{route('index')}}"><img class="" src="{{asset('images/enesmapoSM.png')}}" alt="Logo Enesmapo">ENESMAPO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ENESMAPO
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Mensaje del Director General</a>
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Filosofía</a>
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Organigrama</a>
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Reseña Histórica</a>
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Logotipo y su historia</a>
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Mascota y su historia</a>
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Mural</a>
                        <a class="dropdown-item" href="{{route('enesmapo')}}">Normatividad</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Oferta Educativa
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Lic. Escolarizadas</a>
                        <a class="dropdown-item" href="#">Lic. Semiescolarizadas</a>
                        <a class="dropdown-item" href="#">Posgrado</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Departamentos
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Subdirección Académica</a>
                        <a class="dropdown-item" href="#">Subdirecciión Administrativa</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Planteles
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">San Luis Potosí</a>
                        <a class="dropdown-item" href="#">Ciudad Valles</a>
                        <a class="dropdown-item" href="#">Matehuala</a>
                        <a class="dropdown-item" href="#">Rioverde</a>
                        <a class="dropdown-item" href="#">Tamazunchale</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">SEPA Inglés<span class="sr-only"></span></a>
                </li>
            </ul>
        </div>
            <ul class="navbar-nav navbar-left">
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-facebook"></i><span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-youtube"></i><span class="sr-only"></span></a>
                </li>
            </ul>
    </nav>
</header>