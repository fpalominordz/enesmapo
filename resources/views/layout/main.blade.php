<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="es"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="es"><![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
@yield('meta')
<!--<link rel="shortcut icon" href="{{asset('favicon.ico')}}"/>-->
    <!-- start: MAIN CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- end: MAIN CSS -->
    <!-- google console verification-->

    <!-- font awesome-->
    <script src="https://use.fontawesome.com/255694f056.js"></script>
    @yield('styles')
</head>
<!-- end: HEAD -->
<body>

@include('partials.header')
@yield('mainFeature')
@yield('content')
@include('partials.footer')

<!-- start: MAIN JAVASCRIPTS -->
<!-- google jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<!-- popper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<!-- bootstrap js -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<!-- google analytics -->
<!-- end: MAIN JAVASCRIPTS -->
@yield('scripts')

</body>