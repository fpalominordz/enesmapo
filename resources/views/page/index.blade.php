@extends('layout.main')

@section('meta')
    <title>Home</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>-->
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('mainFeature')
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{asset('images/slide1.jpg')}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('images/slide2.jpg')}}" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('images/slide3.jpg')}}" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
@stop
@section('content')
<section class="bluerplebg">
    <div class="container ene-title">
        <h1>ESCUELA NORMAL DE ESTUDIOS SUPERIORES <br>DEL MAGISTERIO POTOSINO</h1>
    </div>
</section>
<section class="news">
    <div class="container">
        <hr>
        <h1>Noticias</h1><hr>
        <div class="row">
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/noticiasdummy.jpg')}}" alt="Third slide">
                <h4>Noticia1</h4>
                <p>Lorem Ipsum Dolor sit amet.</p>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/noticiasdummy.jpg')}}" alt="Third slide">
                <h4>Noticia1</h4>
                <p>Lorem Ipsum Dolor sit amet.</p>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/noticiasdummy.jpg')}}" alt="Third slide">
                <h4>Noticia1</h4>
                <p>Lorem Ipsum Dolor sit amet.</p>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/noticiasdummy.jpg')}}" alt="Third slide">
                <h4>Noticia1</h4>
                <p>Lorem Ipsum Dolor sit amet.</p>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/noticiasdummy.jpg')}}" alt="Third slide">
                <h4>Noticia1</h4>
                <p>Lorem Ipsum Dolor sit amet.</p>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/noticiasdummy.jpg')}}" alt="Third slide">
                <h4>Noticia1</h4>
                <p>Lorem Ipsum Dolor sit amet.</p>
            </div>
        </div>
    </div>
</section>
<section class="enlaces">
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/moodlelogo.jpg')}}" alt="Third slide">
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/sepalogo.jpg')}}" alt="Third slide">
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/seplogo.jpg')}}" alt="Third slide">
            </div>
        </div>
    </div>
    <hr>
</section>

@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop