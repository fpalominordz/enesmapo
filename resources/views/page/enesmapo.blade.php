@extends('layout.main')

@section('meta')
    <title>enesmapo</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!--<link rel="canonical" href=""/>-->
    <meta name="robots" content="index,follow"/>
    <!-- facebook like snippet
    <meta property="og:locale" content=""/>
    <meta property="og:tittle" content=""/>
    <meta property="og:description" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>-->
    <!-- end: facebook like snippet -->
    <!-- end: META -->
@stop
@section('styles')
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
@stop
@section('mainFeature')

@stop
@section('content')
<section class="bluerplebg">
    <div class="container ene-title">
        <h1>ESCUELA NORMAL DE ESTUDIOS SUPERIORES <br>DEL MAGISTERIO POTOSINO</h1>
    </div>
</section>
<section class="enesmapo">
    <div class="container">
        <hr>
        <h1>ENESMAPO</h1>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/imgdummy.jpg')}}" alt="Third slide">
            </div>
            <div class="col-sm-8">
                <h3>MENSAJE DEL DIRECTOR GENERAL</h3>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam volutpat, orci nec aliquam tincidunt, elit urna commodo augue, tristique lobortis lacus dolor vitae est. Nunc at arcu mi. Sed luctus maximus laoreet. Vivamus ultrices nunc quam, id facilisis leo rhoncus quis. Nam gravida in mauris in congue. Pellentesque fermentum diam ut quam iaculis eleifend. Phasellus quis varius felis. In nibh ante, molestie sit amet varius a, interdum vel arcu.

                   Vestibulum tellus nisl, viverra ut egestas a, posuere ullamcorper sem. Quisque vel ex a felis venenatis bibendum. Nam ultricies egestas libero nec auctor. Praesent neque sem, consequat non suscipit et, ultrices at lorem. In hac habitasse platea dictumst. Maecenas et nulla in tellus iaculis ultrices vitae eu mi. Mauris nec mi et dui eleifend laoreet semper et elit. Integer a dui tempor, ultrices eros et, commodo libero. Nullam ac porttitor massa, in vestibulum libero. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus aliquet maximus tortor ac egestas. Maecenas diam orci, egestas interdum nisl in, eleifend porttitor justo. Nunc dapibus ligula et enim rutrum faucibus. Suspendisse porttitor euismod orci, in aliquet lacus pellentesque eu. Nulla pharetra risus eu eros ultrices ultrices.</p>
            </div>
        </div>
        <div class="row wavebluebg">
            <div class="col-sm-8">
                <h3 class="bluerple">FILOSOFÍA </h3>
                <h4>Misión</h4>
                <p>Proporcionar los servicios educativos de licenciatura en la  modalidad mixta, maestría y doctorado a los profesores de educación básica de escuelas oficiales, para impulsar la superación y el  mejoramiento profesional de los docentes en servicio, a fin de que desarrollen habilidades intelectuales específicas en los rasgos deseables del nuevo maestro en sus perfiles de egreso de  las Licenciaturas en Educación Secundaria Plan 1999 que les permitan dominar los criterios y orientaciones académicas en beneficio de los estudiantes de diferentes niveles educativos.</p>
                <h4>Misión</h4>
                <p>Ser una institución de excelencia en la actualización y superación de profesionales de la educación en la entidad, sustentada en el ejercicio de la democracia y el respeto a la diversidad cultural, con la finalidad de incidir en el mejoramiento de la calidad de la educación básica.
                    Es importante destacar que parte de su planta académica con que se cuenta es personal comisionado, mientras que la mayoría es de contrato, por lo que el seguimiento y el trabajo colegiado se ven afectados.
                </p>
                <p>La ENESMAPO, como centro intelectual tiene como objetivos fundamentales: la creación de nuevos objetos culturales, es decir, la producción de nuevos conocimientos sobre la función educativa en general y las funciones sustantivas de la educación media, en particular, sobre la ciencia, la tecnología, el arte. Tiene el compromiso de cultivar y preservar cualquier manifestación del pensamiento, tomando en cuenta las fuerzas de producción social y la lucha de clases determinantes y detonantes de las formas en que se expresa la creación científica y cultural, en relación estrecha con el proyecto nacional y las necesidades coyunturales. </p>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/imgdummy.jpg')}}" alt="Third slide">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h3>ORGANIGRAMA</h3>
                <img class="d-block w-100" src="{{asset('images/imgdummy.jpg')}}" alt="Third slide">
            </div>
        </div>
        <div class="row wavebluebg">
            <div class="col-sm-8">
                <h3 class="bluerple">RESEÑA HISTÓRICA </h3>
                  <p>La Escuela Normal de Estudios Superiores del Magisterio Potosino, Desde su fundación, se ha hecho presente en el desarrollo de las acciones educativas en nuestro estado, a través de la actualización y superación del profesorado de Educación Básica, Media Superior y Superior. Su origen está determinado por la necesidad de los maestros en servicio en la entidad y desde luego la pertinencia de contar con una institución de estas características. El surgimiento también fue posible por las gestiones y propuestas de la organización sindical y de los sectores educativos, quienes de una manera decidida conformaron una comisión, que a la postre, generarían el proyecto de creación de los que se denomina ENESMAPO.</p>
            <p>Anteriormente los profesionales de la educación, tenían que trasladarse a otras entidades de la republica a estudiar La Normal Superior. Entre las cuales se contaban como las más solicitadas las que funcionaban en el D.F., Puebla, Tlaxcala, Cd. Victoria, Guadalajara, Tepic, Querétaro, Oaxaca, etc. pero el objetivo de los maestros potosinos era contar con una Escuela Normal Superior que funcionara en tierras potosinas y ese anhelo se cumplió en el año de 1993 siendo Gobernador del Estado el Sr. Lic. Horacio Sánchez Unzueta y como Secretario de Educación el Sr. Dr. Humberto Monter Raygadas. El decreto emitido por la Cámara de Diputados del Congreso Potosino cuenta con fecha de 19 de noviembre de 1993.</p>
            <p>Desde entonces la ENESMAPO ha enfrentado una serie de circunstancias favorables y desfavorables, tales como la calidad académica, la presencia en la vida educativa, así como conflictos que se han manifestado en paros estudiantiles, toma de edificios en los que estaba laborando la institución, movilidad de directivos y profesores, todo ello encaminado a superar los obstáculos que impedían la buena marcha de los cinco planteles. Desde luego que no es una institución exenta de diferencias ideológicas y académicas entre sus integrantes, tanto internos como externos. Sin embargo, con una alta dosis de tolerancia y respeto ha sido posible avanzar y superar retos que se han manifestado como producto de la interacción social, política, educativa e ideológica.</p>
            <p>Además la institución siempre ha manifestado con gran respeto hacia las diferentes fuerzas políticas de tal manera que se ha declarado una y mil veces polipartidista dentro de un ámbito de orden y concordia como lo establecen los postulados de la Constitución Política de los Estados Unidos Mexicanos.
                En el Artículo Tercero del Decreto de Creación publicado en el Periódico Oficial del Gobierno del Estado de fecha 19 de noviembre de 1993, se establece que la  Escuela Normal de Estudios Superiores del Magisterio Potosino tendrá como finalidad y objetivos, proporcionar servicios educativos en los grados educativos de Licenciatura, Maestría y Doctorado, para impulsar la nivelación y regularización de los docentes, así como la superación y mejoramiento profesionales del personal al servicio de la Secretaría de Educación del Gobierno del Estado, tanto en el beneficio como en el óptimo nivel de la educación, así como en el de la sociedad en general”.
            </p>
            <p>En 1993, el entonces Gobernador Constitucional del Estado, Lic. Horacio Sánchez Unzueta, determina la apertura en las Licenciaturas por áreas, en ese entonces, en Ciencias Sociales, Ciencias Naturales, Inglés, Matemáticas, Español, Educación Física y Educación Especial (problemas de aprendizaje).  Durante el periodo 1993 al 2000, esta institución marco rumbos de trabajo en diferentes planteles educativos en el interior del estado: de Rioverde, Cd. Valles, Tamazunchale y Matehuala, como sedes foráneas, así como en esta capital.  A partir de ese momento, han desfilado un sin número de profesores que recibieron el beneficio de regularizarse y enfrentar sus responsabilidades con una adecuada formación de docentes.</p>
            <p>En la actualidad la institución ofrece Licenciaturas en Educación Secundaria en la modalidad Mixta con especialidad en Español, Inglés, Telesecundaria, Física, Formación Cívica y Ética, Matemáticas, Biología e Historia, además de contar con la Licenciatura en educación Primaria en la modalidad Escolarizada y La Maestría en Educación Secundaria Campo: Diseño de Estrategias Didácticas. A la fecha han egresado 36 generaciones de Licenciados.</p>
            <h4>EDIFICIOS QUE HA OCUPADO LA INSTITUCIÓN:</h4>
                <ul>
                    <li>ESCUELA PRIMARIA JAVIER BARROS SIERRA</li>
                    <li>ESCUELA SECUNDARIA TECNICA · 68</li>
                    <li>ESCUELA SECUNDARIA GENERAL DIONISIO ZAVALA</li>
                    <li>EN LAS INSTALACIONES ACTUALES DESDE 2002.</li>
                </ul>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/imgdummy.jpg')}}" alt="Third slide">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/enesmapologofull.jpg')}}" alt="Third slide">
            </div>
            <div class="col-sm-8">
                <h3>LOGOTIPO Y SU HISTORIA</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam volutpat, orci nec aliquam tincidunt, elit urna commodo augue, tristique lobortis lacus dolor vitae est. Nunc at arcu mi. Sed luctus maximus laoreet. Vivamus ultrices nunc quam, id facilisis leo rhoncus quis. Nam gravida in mauris in congue. Pellentesque fermentum diam ut quam iaculis eleifend. Phasellus quis varius felis. In nibh ante, molestie sit amet varius a, interdum vel arcu.

                    Vestibulum tellus nisl, viverra ut egestas a, posuere ullamcorper sem. Quisque vel ex a felis venenatis bibendum. Nam ultricies egestas libero nec auctor. Praesent neque sem, consequat non suscipit et, ultrices at lorem. In hac habitasse platea dictumst. Maecenas et nulla in tellus iaculis ultrices vitae eu mi. Mauris nec mi et dui eleifend laoreet semper et elit. Integer a dui tempor, ultrices eros et, commodo libero. Nullam ac porttitor massa, in vestibulum libero. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus aliquet maximus tortor ac egestas. Maecenas diam orci, egestas interdum nisl in, eleifend porttitor justo. Nunc dapibus ligula et enim rutrum faucibus. Suspendisse porttitor euismod orci, in aliquet lacus pellentesque eu. Nulla pharetra risus eu eros ultrices ultrices.</p>
            </div>
        </div>
        <div class="row wavebluebg">
            <div class="col-sm-8">
                <h3 class="bluerple">MASCOTA Y SU HISTORIA</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam volutpat, orci nec aliquam tincidunt, elit urna commodo augue, tristique lobortis lacus dolor vitae est. Nunc at arcu mi. Sed luctus maximus laoreet. Vivamus ultrices nunc quam, id facilisis leo rhoncus quis. Nam gravida in mauris in congue. Pellentesque fermentum diam ut quam iaculis eleifend. Phasellus quis varius felis. In nibh ante, molestie sit amet varius a, interdum vel arcu.

                    Vestibulum tellus nisl, viverra ut egestas a, posuere ullamcorper sem. Quisque vel ex a felis venenatis bibendum. Nam ultricies egestas libero nec auctor. Praesent neque sem, consequat non suscipit et, ultrices at lorem. In hac habitasse platea dictumst. Maecenas et nulla in tellus iaculis ultrices vitae eu mi. Mauris nec mi et dui eleifend laoreet semper et elit. Integer a dui tempor, ultrices eros et, commodo libero. Nullam ac porttitor massa, in vestibulum libero. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus aliquet maximus tortor ac egestas. Maecenas diam orci, egestas interdum nisl in, eleifend porttitor justo. Nunc dapibus ligula et enim rutrum faucibus. Suspendisse porttitor euismod orci, in aliquet lacus pellentesque eu. Nulla pharetra risus eu eros ultrices ultrices.</p>
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/mascota.jpg')}}" alt="Third slide">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/mural.jpg')}}" alt="Third slide">
            </div>
            <div class="col-sm-8">
                <h3>MURAL</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam volutpat, orci nec aliquam tincidunt, elit urna commodo augue, tristique lobortis lacus dolor vitae est. Nunc at arcu mi. Sed luctus maximus laoreet. Vivamus ultrices nunc quam, id facilisis leo rhoncus quis. Nam gravida in mauris in congue. Pellentesque fermentum diam ut quam iaculis eleifend. Phasellus quis varius felis. In nibh ante, molestie sit amet varius a, interdum vel arcu.

                    Vestibulum tellus nisl, viverra ut egestas a, posuere ullamcorper sem. Quisque vel ex a felis venenatis bibendum. Nam ultricies egestas libero nec auctor. Praesent neque sem, consequat non suscipit et, ultrices at lorem. In hac habitasse platea dictumst. Maecenas et nulla in tellus iaculis ultrices vitae eu mi. Mauris nec mi et dui eleifend laoreet semper et elit. Integer a dui tempor, ultrices eros et, commodo libero. Nullam ac porttitor massa, in vestibulum libero. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus aliquet maximus tortor ac egestas. Maecenas diam orci, egestas interdum nisl in, eleifend porttitor justo. Nunc dapibus ligula et enim rutrum faucibus. Suspendisse porttitor euismod orci, in aliquet lacus pellentesque eu. Nulla pharetra risus eu eros ultrices ultrices.</p>
            </div>
        </div>
    </div>
</section>
<section class="enlaces">
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/moodlelogo.jpg')}}" alt="Third slide">
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/sepalogo.jpg')}}" alt="Third slide">
            </div>
            <div class="col-sm-4">
                <img class="d-block w-100" src="{{asset('images/seplogo.jpg')}}" alt="Third slide">
            </div>
        </div>
    </div>
    <hr>
</section>

@stop
@section('scripts')
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
@stop